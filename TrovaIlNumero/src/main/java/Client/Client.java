package Client;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

public class Client {

    static Socket socketServer = null;
    public static final int PORT = 9999;   //port's number

    static DataInputStream in;
    static DataOutputStream out;
    Scanner sc = new Scanner(System.in);

    //create connection between client and server
    public Socket connetti(){
        Socket server = null;
        try{
            System.out.println("Try connecting to server...");
            server = new Socket("localhost", PORT);
            System.out.println("Connection established");
            in = new DataInputStream(server.getInputStream());
            out = new DataOutputStream(server.getOutputStream());
        } catch(UnknownHostException e){
            System.err.println("Unknown host");
        } catch (Exception e) {
            System.err.println("Impossible to establish a connection");
            System.exit(1);
        }
        return server;
    }

    //method involved in guessing the number
    public String guess(){
        String s = "";
        System.out.print("Try to guess the number: ");
        s = sc.nextLine();
        return s;
    }

    //check if the user's input string format is correct
    private static boolean isRight(String s){
        ArrayList<Character> arrayList = new ArrayList<>();
        for (char c : s.toCharArray()) {
            if(arrayList.contains(c))
                return false;
            else
                arrayList.add(c);
        }
        for(char c : s.toCharArray()){
            if(!Character.isDigit(c))
                return false;
        }
        return true;
    }

    //method involved in processing the answer coming from the server
    public String readServer(){
        String s = "";
        try {
            s = in.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(s.equals("You guessed!")) {
            System.out.println(s);
            System.exit(0);
        }
        return s;
    }

    //main method
    public static void main(String[] args) {
        Client c = new Client();
        socketServer = c.connetti();
        try {
            in = new DataInputStream(new BufferedInputStream(socketServer.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true) {
            String s = c.guess();
            if(isRight(s)){
                try {
                    out.writeUTF(s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(c.readServer());
            } else {
                System.out.println("Wrong input!");
            }
        }
    }
}
