package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static final int PORT = 9999;    //port's number
    static Socket socket = null;
    static ServerSocket serverSocket = null;

    //main method which accept the connection between server and client
    public static void main(String[] args) {
        try{
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true){
            try{
                socket = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ServerThread serverThread = new ServerThread(socket);
            serverThread.start();
        }
    }
}
