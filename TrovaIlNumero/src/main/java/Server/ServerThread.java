package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class ServerThread extends Thread {

    String magicNumber = "";    //mysterious number in String format
    Socket socketClient = null;

    DataInputStream in;
    DataOutputStream out;

    private ArrayList<Integer> number = new ArrayList<>();  //mysterious number in ArrayList format

    //ServerThread's constructor
    protected ServerThread(Socket socket){
        socketClient = socket;
        number = generateNumber();
        magicNumber = convertInput(number);
    }

    //method involved in acquiring client's request
    public String readClient(){
        try{
            in = new DataInputStream(socketClient.getInputStream());
        } catch(Exception e){
            e.printStackTrace();
        }

        String s = "";
        try {
            s = in.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    //game method which overrides Thread.run()
    @Override
    public void run(){
        try{
            out = new DataOutputStream(socketClient.getOutputStream());
        }catch(Exception e){
            e.printStackTrace();
        }
        System.out.println(magicNumber);
        while(true){
            String clientRead = readClient();
            int p = checkPosition(number, convertInput(clientRead));
            int n = checkNumber(number, convertInput(clientRead));
            if(clientRead.equals(magicNumber)){
                try {
                    out.writeUTF("You guessed!");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            } else {
                try {
                    out.writeUTF("Numeri in posizione giusta: " + p + "\n" + "Numeri presenti ma in posizione sbagliata: " + (n - p) + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //method which checks if the positions of the number acquired as input are correct
    private int checkPosition(ArrayList<Integer> magicNum, ArrayList<Integer> userNum){
        int ret = 0;
        for(int i = 0; i < 4; i++){
            if(magicNum.get(i) == userNum.get(i))
                ret++;
        }
        return ret;
    }

    //method which checks if any digits of the number acquired as input are present into the mysterious number
    private int checkNumber(ArrayList<Integer> magicNum, ArrayList<Integer> userNum){
        int ret = 0;
        for(Integer i : userNum){
            if(magicNum.contains(i))
                ret++;
        }
        return ret;
    }

    //methos used to convert the input's format
    private ArrayList<Integer> convertInput(String s){
        ArrayList<Integer> ret = new ArrayList<>();
        for(int i = 0; i < s.length(); i++)
            ret.add(Character.getNumericValue(s.charAt(i)));
        return ret;
    }

    //methos used to convert the input's format
    private String convertInput(ArrayList<Integer> list){
        String ret = "";
        for(Integer i : list){
            ret += i.toString();
        }
        return ret;
    }

    //method used to generate the mysterious number
    private ArrayList<Integer> generateNumber(){
        int n = 0;
        while(number.size() < 4){
            Random rnd = new Random();
            n = rnd.nextInt(10);
            if(!number.contains(n))
                number.add(n);
        }
        return number;
    }
}